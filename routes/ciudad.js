// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de ciudad
var ciudad_controller = require('../controllers/ciudad');

// GET /:id
router.get('/:id', ciudad_controller.ciudad_details);
// GET /
router.get('/', ciudad_controller.ciudad_all);
// POST /
router.post('/', ciudad_controller.ciudad_create);
// PUT /:id
router.put('/:id', ciudad_controller.ciudad_update);
// DELETE /:id
router.delete('/:id', ciudad_controller.ciudad_delete);

//export router
module.exports = router;