// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de persona
var persona_controller = require('../controllers/persona');

// GET /:id
router.get('/:id', persona_controller.persona_details);
// GET /
router.get('/', persona_controller.persona_all);
// POST /
router.post('/', persona_controller.persona_create);
// PUT /:id
router.put('/:id', persona_controller.persona_update);
// DELETE /:id
router.delete('/:id', persona_controller.persona_delete);
// PUT ciudad 
router.put('/ciudad/:id',persona_controller.Persona_add_ciudad);

//export router
module.exports = router;