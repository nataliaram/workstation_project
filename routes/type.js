var express = require('express');
var router = express.Router();

var typeController = require('../controllers/type');

router.get('/:id', typeController.typeShow);
router.get('/', typeController.typeAll);
router.post('/', typeController.typeCreate);
router.put('/:id', typeController.typeUpdate);
router.delete('/:id', typeController.typeDelete);

module.exports = router;
