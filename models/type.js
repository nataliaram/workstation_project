var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TypeSchema = new Schema({
  name: {type:String, required:true},
  description: {type:String, required:true},
});

TypeSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Type', TypeSchema);
