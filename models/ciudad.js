//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Ciudad
var CiudadSchema = new Schema({
nombre: {type:String, required:true}, 
region: {type:String, required:true}, 

});

CiudadSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Ciudad', CiudadSchema);