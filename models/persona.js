//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Persona
var PersonaSchema = new Schema({
nombre: {type:String, required:true}, 
direccion: {type:String, required:true}, 
telefono: {type:Number, required:true}, 
ciudad:[ {type:Schema.Types.ObjectId, ref: 'Ciudad' , autopopulate: true }], 

});

PersonaSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Persona', PersonaSchema);