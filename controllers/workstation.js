var Workstation = require('../models/workstation');

exports.workstationCreate = function (req, res) {
  var workstation = new Workstation(req.body);

  workstation.save(function (err, workstationInstance) {
    if (err) {
      res.send({status: "Err", error:err})
    }
    res.send({status: "created", value: workstationInstance})
  });
};

exports.workstationShow = function (req, res) {
  Workstation.findById(req.params.id, function (err, workstation) {
    if (err) {
      res.send({status: "Err", error:err});
    }
    res.send(workstation);
  })
};

exports.workstationAll = function (req, res) {
  Workstation.find(req.params.id, function (err, workstations) {
    if (err) {
      res.send({status: "Err", error:err})
      return next(err);
    }
    res.send(workstations);
  })
};

exports.workstationUpdate = function (req, res) {
  Workstation.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, workstation) {
    if (err) {
      res.send({status: "Err", error:err})
      return next(err);
    }
    res.send({status: "updated", value: workstation });
  });
};

exports.workstationDelete = function (req, res) {
  Workstation.findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      res.send({status: "Err", error:err})
      return next(err);
    }
    res.send({status:"deleted"});
  })
};
