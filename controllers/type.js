var Type = require('../models/type');

exports.typeCreate = function (req, res) {
  var type = new Type(req.body);

  type.save(function (err, typeInstance) {
    if (err) {
      res.send({status: "Err", error:err})
    }
    res.send({status: "created", value: typeInstance})
  });
};

exports.typeShow = function (req, res) {
  Type.findById(req.params.id, function (err, type) {
    if (err) {
      res.send({status: "Err", error:err});
    }
    res.send(type);
  })
};

exports.typeAll = function (req, res) {
  Type.find(req.params.id, function (err, types) {
    if (err) {
      res.send({status: "Err", error:err})
      return next(err);
    }
    res.send(types);
  })
};

exports.typeUpdate = function (req, res) {
  Type.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, type) {
    if (err) {
      res.send({status: "Err", error:err})
      return next(err);
    }
    res.send({status: "updated", type: type });
  });
};

exports.typeDelete = function (req, res) {
  Type.findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      res.send({status: "Err", error:err})
      return next(err);
    }
    res.send({status:"deleted"});
  })
};
