var superagent = require('superagent')
var expect = require('expect.js')

describe('Test Worstation', function () {
  var id
  var url = 'http://127.0.0.1:3002';

  it('Test POST /workstation ', function (done) {
    superagent.post(url + '/workstation')
      .send({
      	name: "Equipo 4",
      	type: "5ce750a1d4229628c4b50dc7",
      	laboratory: "1",
      	implement: "2"
      })
      .end(function (e, res) {
        id = res.body.value._id;
        expect(e).to.eql(null)
        done()
      })
  })

  it('Test GET /workstation', function (done) {
    superagent.get(url + '/workstation')
      .end(function (e, res) {

        expect(e).to.eql(null)
        done()
      })
  })

  it('test GET /workstation/:id', function (done) {
    superagent.get(url + '/workstation/'+id)
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body._id).to.eql(id)
        done()
      })
  })

  // DELETE /tipo/:id
  it('Test DELETE /workstation/:id ', function (done) {
    superagent.delete(url +'/workstation/' + id)
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body.status).to.eql('deleted')
        done()
      })
  })


});
