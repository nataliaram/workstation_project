var superagent = require('superagent')
var expect = require('expect.js')

describe('Test Type ', function () {
  var id
  var url = 'http://127.0.0.1:3002';

  it('Test POST /type ', function (done) {
    superagent.post(url + '/type')
      .send({
      	name: "Equipo 4",
      	description: "Description"
      })
      .end(function (e, res) {
        id = res.body.value._id;
        expect(e).to.eql(null)
        done()
      })
  })

  it('Test GET /type', function (done) {
    superagent.get(url + '/type')
      .end(function (e, res) {

        expect(e).to.eql(null)
        done()
      })
  })

  it('test GET /type/:id', function (done) {
    superagent.get(url + '/type/'+id)
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body._id).to.eql(id)
        done()
      })
  })

  // DELETE /tipo/:id
  it('Test DELETE /type/:id ', function (done) {
    superagent.delete(url +'/type/' + id)
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body.status).to.eql('deleted')
        done()
      })
  })


});
